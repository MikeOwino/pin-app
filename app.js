const express = require("express");
const bcrypt = require("bcrypt");

const app = express();
const PORT = 3000;

app.use(express.urlencoded({ extended: true }));

// Serve the login page
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/login.html");
});

// Handle the login form submission
app.post("/login", async (req, res) => {
  const { pin } = req.body;

  // Hash the PIN using bcrypt
  const hashedPin = await bcrypt.hash(pin, 10);

  // Store the hashed PIN in a cookie
  res.cookie("pin", hashedPin, { httpOnly: true });

  // Redirect the user to the main website
  res.redirect("/main");
});

// Serve the main website
app.get("/main", (req, res) => {
  const { pin } = req.cookies;

  // If the PIN cookie is not set, redirect the user to the login page
  if (!pin) {
    res.redirect("/");
    return;
  }

  // Otherwise, compare the stored hashed PIN with the user's input
  bcrypt.compare(req.query.pin, pin, (err, result) => {
    if (err || !result) {
      // If the comparison fails, redirect the user to the login page
      res.redirect("/");
    } else {
      // Otherwise, serve the main website
      res.send("Welcome to the main website!");
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
